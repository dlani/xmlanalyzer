

class AnalyzerError(Exception):
    pass


class FeatureNotFoundError(AnalyzerError):
    pass


class IncorrectFileFormat(AnalyzerError):
    pass
