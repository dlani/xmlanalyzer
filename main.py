import argparse
import sys

from constants import HTML_FORMAT
from exceptions import IncorrectFileFormat
from finder import finder


def validate_input_files(*input_files):
    for each in input_files:
        if not each.split(".")[-1] == HTML_FORMAT:
            raise IncorrectFileFormat(f"The incoming file must be in the {HTML_FORMAT} format")


def parse_args():
    parser = argparse.ArgumentParser(description='HTML Analyzer')
    parser.add_argument('origin_file_path', type=str, help='an input html file path')
    parser.add_argument('updated_file_path', type=str, help='an input diff html file path')
    args = parser.parse_args()
    validate_input_files(args.origin_file_path, args.updated_file_path)
    return args.origin_file_path, args.updated_file_path


def get_path(origin_file_path, updated_file_path):
    features = finder.get_features_from_origin(origin_file_path)
    return finder.extract_path(features, updated_file_path)


if __name__ == "__main__":
    try:
        origin_file_path, updated_file_path = parse_args()
        path = get_path(origin_file_path, updated_file_path)
        sys.stdout.write("PATH FOUND: " + path + "\n")
        sys.exit(0)
    except Exception as e:
        sys.stdout.write(f"Exception occurred: {str(e)}" + "\n")
        sys.exit(1)
