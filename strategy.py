import operator
from abc import ABC, abstractmethod
from typing import List, Dict

from lxml.html import HtmlElement

from constants import EXCLUDE_FEATURE_TAGS
from logger import logger


class AbstractFeatureFindStrategy(ABC):
    """
    Define the general interface for searching for features in updated files and the rule element selection strategy
    """

    @abstractmethod
    def build_predicate(self, features: Dict) -> str:
        """
        build predicate to search in xpath
        :param features: feature dictionary
        :return:
        """
        pass

    @abstractmethod
    def get_features_from_element(self, element: HtmlElement) -> Dict:
        """
        Extracts attributes from an element and adds text element
        :param element:
        :return:
        """
        pass

    @abstractmethod
    def get_element(self, elements: List, origin_features: Dict) -> HtmlElement:
        """
        Return element with maximum similarity to origin features
        :param elements:
        :param origin_features: features by which maximum similarity is determined
        :return:
        """
        pass


class MostSimilarFeatureFindStrategy(AbstractFeatureFindStrategy):

    def build_predicate(self, features: Dict) -> str:
        # can be improved
        attributes_feature = (f'@{k}="{v}"' if k != "text()" else f'{k}="{v}"'
                              for k, v in features.items() if k not in EXCLUDE_FEATURE_TAGS)
        return './/a[' + ' or '.join(attributes_feature) + ']'

    def get_features_from_element(self, element: HtmlElement) -> Dict:
        attributes_feature = {k: v for k, v in element.attrib.items() if k not in EXCLUDE_FEATURE_TAGS}
        attributes_feature.update({"text()": element.text.strip()})
        return dict(attributes_feature)

    def get_element(self, elements: List, origin_features: Dict) -> HtmlElement:
        logger.debug("Starting the search most similar to the origin features")
        feature_diffs_count = {}
        for each in elements:
            diff_features = self.get_features_from_element(each)
            logger.debug(f"Features of a similar element {diff_features}")
            feature_diffs_count[each] = origin_features.items() - diff_features.items()
        return min(feature_diffs_count.items(), key=operator.itemgetter(1))[0]

