import logging
import sys

from constants import (
    LOG_FORMAT,
    LOG_LEVEL,
    LOG_NAME
)

logger = logging.getLogger(LOG_NAME)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter(LOG_FORMAT)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(LOG_LEVEL)
