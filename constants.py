import logging

ORIGIN_FILE_PATH = 'original.html'
FIRST_DIFF_FILE_PATH = 'first.html'
SECOND_DIFF_FILE_PATH = 'second.html'
THIRD_DIFF_FILE_PATH = 'third.html'
FORTH_DIFF_FILE_PATH = 'forth.html'
INCORRECT_FILE_FORMAT_PATH = 'incorrect_format.htm'
NO_ELEMENT_FOUND_FILE_PATH = 'no_elements_found.html'

EXCLUDE_FEATURE_TAGS = ('rel', 'onclick', 'id')
ORIGIN_FEATURE_XPATH = './/a[@id="make-everything-ok-button"]'

HTML_FORMAT = 'html'

LOG_NAME = 'ANALYZER_LOGGER'
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = "%(name)s: %(asctime)s %(levelname)8s %(message)s "
