analyzer service
======================

Checkout
---
    git clone https://dlani@bitbucket.org/dlani/xmlanalyzer.git
    
install requirements 
---

    pip install -r requirements.txt

Running
---
    python3 xmlanalyzer/main.py <project>/tests/fixtures/original.html <project>/tests/fixtures/first.html

Expected result: PATH: /html/body/div/div/div[3]/div[1]/div/div[2]/a[2].

Running tests
---
    python3 -m pytest xmlanalyzer/tests/
