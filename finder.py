from typing import Dict

from constants import ORIGIN_FEATURE_XPATH
from lxml import html
from lxml.etree import ElementTree

from exceptions import FeatureNotFoundError
from logger import logger
from strategy import MostSimilarFeatureFindStrategy


class FeatureFinder:
    """
    Looking for the most suitable element according to a specific strategy
    """

    def __init__(self, strategy):
        self.strategy = strategy

    def get_element_tree(self, file_path: str) -> ElementTree:
        with open(file_path) as file:
            return ElementTree(html.fromstring(file.read()))

    def get_features_from_origin(self, file_path: str) -> Dict:
        """
        Extracts tags from the source file
        :param file_path:
        :return:
        """
        logger.debug("Start searching for features from origin")
        element_tree = self.get_element_tree(file_path)
        elements = element_tree.xpath(ORIGIN_FEATURE_XPATH)
        try:
            attributes_feature = self.strategy.get_features_from_element(elements[0])
        except IndexError:
            raise FeatureNotFoundError(f'No original features was found for path {ORIGIN_FEATURE_XPATH}')
        logger.debug(f"Features that where found {attributes_feature}")
        return attributes_feature

    def extract_path(self, features, updated_file_path):
        """
        Extracts path of similar element by features from the source file
        :param features:
        :param updated_file_path:
        :return:
        """
        element_tree = self.get_element_tree(updated_file_path)
        elements = element_tree.xpath(self.strategy.build_predicate(features))
        if not elements:
            raise FeatureNotFoundError(f'No similar futures was found for path {updated_file_path}')
        element = self.strategy.get_element(elements, features)
        logger.debug(f"Found item {element.attrib}")
        return element_tree.getpath(element)


finder = FeatureFinder(MostSimilarFeatureFindStrategy())
