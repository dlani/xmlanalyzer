import argparse

import pytest
import os

from constants import (
    ORIGIN_FILE_PATH,
    FIRST_DIFF_FILE_PATH,
    SECOND_DIFF_FILE_PATH,
    THIRD_DIFF_FILE_PATH,
    FORTH_DIFF_FILE_PATH,
    INCORRECT_FILE_FORMAT_PATH,
    HTML_FORMAT,
    NO_ELEMENT_FOUND_FILE_PATH
)
from exceptions import IncorrectFileFormat, FeatureNotFoundError
from main import get_path, parse_args


def fixture(file_name):
    return os.path.join(os.path.join(os.path.dirname(__file__), "fixtures"), file_name)


@pytest.mark.parametrize("input_file, diff, expected", [
        (fixture(ORIGIN_FILE_PATH), fixture(FIRST_DIFF_FILE_PATH), '/html/body/div/div/div[3]/div[1]/div/div[2]/a[2]'),
        (fixture(ORIGIN_FILE_PATH), fixture(SECOND_DIFF_FILE_PATH), '/html/body/div/div/div[3]/div[1]/div/div[2]/div/a'),
        (fixture(ORIGIN_FILE_PATH), fixture(THIRD_DIFF_FILE_PATH), '/html/body/div/div/div[3]/div[1]/div/div[3]/a'),
        (fixture(ORIGIN_FILE_PATH), fixture(FORTH_DIFF_FILE_PATH), '/html/body/div/div/div[3]/div[1]/div/div[3]/a'),
])
def test_get_path(input_file, diff, expected):
    assert get_path(input_file, diff) == expected


def test_feature_not_found_error():
    with pytest.raises(FeatureNotFoundError):
        get_path(fixture(ORIGIN_FILE_PATH), fixture(NO_ELEMENT_FOUND_FILE_PATH))


def test_incorrect_file_format(mocker):
    mocker.patch('argparse.ArgumentParser.parse_args',
                 return_value=argparse.Namespace(origin_file_path=ORIGIN_FILE_PATH,
                                                 updated_file_path=INCORRECT_FILE_FORMAT_PATH))
    with pytest.raises(IncorrectFileFormat, match=f"The incoming file must be in the {HTML_FORMAT} format"):
        parse_args()
